# Gnomebin

Horribly insecure pastebin clone that shows ads before showing a paste, defeating the whole point of a pastebin.

Oh and yeah I used cookies unique to the paste name to mark if a user viewed ads or not, lol. It's VERY easy to bypass. Like, you could load the ad page, refresh and it'd be away, or you could just inject the cookies.

this isn't a serious project. it works as a joke, but it's not recommended for production.

If you want to use it, copy `config.py.template` to `config.py`, configure it and install sanic through pip3. Oh and yeah, you'll need python 3.6+. Have fun.

## Video

![](https://awo.oooooooooooooo.ooo/i/bh909k9t.webm)

## But why

!["just serve an ad before the plain text and you cant execute it :BigBrain:"](https://awo.oooooooooooooo.ooo/i/lrlavegk.png)

!["okay" "okay" "okay" "I'll make an ad supported pastebin" "is everyone ready"](https://awo.oooooooooooooo.ooo/i/56dui4eu.png)

!["It'll be great" ":)"](https://awo.oooooooooooooo.ooo/i/t7a1w15j.png)

It did end up being great, didn't it?
